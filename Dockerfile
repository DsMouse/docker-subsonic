FROM ubuntu:18.04
ENV LANG="en_US.UTF-8" s6_overlay_version="1.17.1.2" APP_NAME="subsonic" IMG_NAME="subsonic"  S6_LOGGING="0"


RUN true \
 && apt update -y \
 && apt install -y  \
        file \
        curl \
        openjdk-8-jre-headless \
	ffmpeg \
	tar \
  && echo $(curl -skL https://sourceforge.net/projects/subsonic/rss |grep link|grep download|grep tar.gz|grep -iv beta|sed -n s%"\s\+<link>\([^<]*\).*"%"\1"%p|head -1|awk -F/ '{print $8}') > /subsonic.version \
  &&  curl -skL $(curl -skL https://sourceforge.net/projects/subsonic/rss |grep link|grep download|grep tar.gz|grep -iv beta|sed -n s%"\s\+<link>\([^<]*\).*"%"\1"%p|head -1|sed s%/download$%%) -o /tmp/subsonic.tar.gz \
  && mkdir -p /opt/subsonic \
  && tar xf /tmp/subsonic.tar.gz -C /opt/subsonic \
  && ls /usr/lib/locale | grep -ve "en_CA*\|en_US*" | awk '{if(system("[ -f "$1" ]") == 0) {print $1} }' | xargs rm -rf \
  && ( rm -rf /usr/share/{man,doc,info,gnome/help} || true ) \
  && ( rm -rf /tmp/* || true ) \
  && ( rm -rf /var/log/* || true )

COPY root /
ENTRYPOINT ["/start.sh"]
